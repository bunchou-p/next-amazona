/* eslint-disable no-case-declarations */
import { createContext, useReducer } from 'react';
import Cookies from 'js-cookie';

export const Store = createContext();

const initialState = {
    darkMode: Cookies.get('darkMode') === 'ON' ? true : false,
    cart: {
        cartItems: Cookies.get('cartItems') ? JSON.parse(Cookies.get('cartItems')) : [],
        shippingAddress: Cookies.get('shippingAddress') ? JSON.parse(Cookies.get('shippingAddress')) : { location: {} },
        paymentMethod: Cookies.get('paymentMethod') ? Cookies.get('paymentMethod') : '',
    },
    userInfo: Cookies.get('userInfo') ? JSON.parse(Cookies.get('userInfo')) : null,
};

function reducer(state, action) {
    const { type, payload } = action;
    switch (type) {
        case 'DARK_MODE_ON':
            return { ...state, darkMode: true };
        case 'DARK_MODE_OFF':
            return { ...state, darkMode: false};
        case 'CART_ADD_ITEM':
            const existItem = state.cart.cartItems.find(
                (item) => item._id === payload._id
            );
            const cartItems = existItem
                ? state.cart.cartItems.map((item) => item._id === existItem._id ? payload : item)
                : [...state.cart.cartItems, payload];
            Cookies.set('cartItems', JSON.stringify(cartItems));
            return { ...state, cart: { ...state.cart, cartItems } };
        case 'CART_REMOVE_ITEM':
            const cartFilteredItems = state.cart.cartItems.filter(
                (item) => item._id !== payload._id
            );
            Cookies.set('cartItems', JSON.stringify(cartFilteredItems));
        case 'SAVE_SHIPPING_ADDRESS_MAP_LOCATION':
            return {
                ...state,
                cart: {
                ...state.cart,
                shippingAddress: {
                    ...state.cart.shippingAddress,
                    location: action.payload,
                },
                },
            };
            return { ...state, cart: { ...state.cart, cartItems: cartFilteredItems } };
        case 'SAVE_SHIPPING_ADDRESS':
            return { ...state, cart: { ...state.cart, shippingAddress: payload }};
        case 'SAVE_PAYMENT_METHOD':
            return { ...state, cart: { ...state.cart, paymentMethod: payload }};
        case 'CART_CLEAR':
            return { ...state, cart: { ...state.cart, cartItems: [] } };
        case 'USER_LOGIN':
            return { ...state, userInfo: payload };
        case 'USER_LOGOUT':
            return {
                ...state,
                userInfo: null,
                cart: {
                    cartItems: [],
                    shippingAddress: { location: {} },
                    paymentMethod: '',
                },
            };
        default:
            return state;
    }
}

export function StoreProvider(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = { state, dispatch };
    return <Store.Provider value={value}>{props.children}</Store.Provider>
}
