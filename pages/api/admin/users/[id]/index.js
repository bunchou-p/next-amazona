import nc from 'next-connect';
import User from '../../../../../models/User';
import db from '../../../../../utils/db';
import { isAuth, isAdmin } from '../../../../../utils/auth';

const handler = nc({
    onError: db.onError,
});

handler.use(isAuth, isAdmin);

handler.get(async(req, res) => {
    await db.connect();
    const user = await User.findById(req.query.id);
    await db.disconnect();
    res.send(user);
});

handler.delete(async(req, res) => {
    await db.connect();
    const user = await User.findById(req.query.id);
    if (user) {
        await user.remove();
        await db.disconnect();
        res.send({ message: 'User deleted' });    
    } else {
        await db.disconnect();
        res.status(404).send({ message: 'User not found.' });
    }
});

handler.put(async(req, res) => {
    await db.connect();
    const user = await User.findById(req.query.id);
    if (user) {
        user.name = req.body.name;
        user.isAdmin = Boolean(req.body.isAdmin);
        await user.save();
        await db.disconnect();
        res.send({ message: 'User updated' });
    } else {
        await db.disconnect();
        res.status(404).send({ message: 'User not found.' });
    }
});

export default handler;