import nc from 'next-connect';
import multer from 'multer';
import { v2 as cloudinary } from 'cloudinary';
import stramifier from 'streamifier';

import db from '../../../utils/db';
import { isAuth, isAdmin } from '../../../utils/auth';

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET,
})

const upload = multer();

export const config = {
    api: {
        bodyParser: false,
    },
};

const handler = nc({
    onError: db.onError,
});

handler.use(isAuth, isAdmin, upload.single('file')).post(async(req, res) => {
    // create streamUpload function
    const streamUpload = (req) => {
        return new Promise((resolve, reject) => {
            const stream = cloudinary.uploader.upload_stream((error, result) => {
                if (result) {
                    resolve(result);
                } else {
                    reject(error);
                }
            });
            stramifier.createReadStream(req.file.buffer).pipe(stream);
        })
    };
    const result = await streamUpload(req);
    res.send(result);
});

export default handler;