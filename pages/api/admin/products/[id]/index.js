import nc from 'next-connect';
import Product from '../../../../../models/Product';
import db from '../../../../../utils/db';
import { isAuth, isAdmin } from '../../../../../utils/auth';

const handler = nc({
    onError: db.onError,
});

handler.use(isAuth, isAdmin);

handler.get(async(req, res) => {
    await db.connect();
    const product = await Product.findById(req.query.id);
    await db.disconnect();
    res.send(product);
});

handler.delete(async(req, res) => {
    await db.connect();
    const product = await Product.findById(req.query.id);
    if (product) {
        await product.remove();
        await db.disconnect();
        res.send({ message: 'Product deleted' });    
    } else {
        await db.disconnect();
        res.status(404).send({ message: 'Product not found.' });
    }
});

handler.put(async(req, res) => {
    await db.connect();
    const product = await Product.findById(req.query.id);
    if (product) {
        product.name = req.body.name;
        product.slug = req.body.slug;
        product.price = req.body.price;
        product.image = req.body.image;
        product.featuredImage = req.body.featuredImage;
        product.isFeatured = req.body.isFeatured;    
        product.category = req.body.category;
        product.brand = req.body.brand;
        product.countInStock = req.body.countInStock;
        product.description = req.body.description;
        await product.save();
        await db.disconnect();
        res.send({ message: 'Product updaed successfully.'});
    } else {
        await db.disconnect();
        res.status(404).send({ message: 'Product not found.' })
    }
});

export default handler;